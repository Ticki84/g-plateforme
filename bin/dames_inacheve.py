from tkinter import*
from random import*

#def startSnake(fen, frame, btnRetour):
def mov():
    global idle, objIdx, objArr, cx, cy, tx, ty, ccx, ccy, px, py
    idle = False
    dx = 0
    dy = 0
    if ccx == -1:
        ccx = 32*cx
    if ccy == -1:
        ccy = 32*cy
    if ccx < tx:
        dx = 2
        ccx += 2
    elif ccx > tx:
        dx = -2
        ccx -= 2
    if ccy < ty:
        dy = 2
        ccy += 2
    elif ccy > ty:
        dy = -2
        ccy -= 2
    zone_dessin.move(objArr[objIdx][0], dx, dy)
    if (ccx == 32*px) & (ccy == 32*py):
        for i in range(0,len(objArr)):
            if (objArr[i][1] == px) & (objArr[i][2] == py):
                zone_dessin.delete(objArr[i][0])
                del objArr[i]
                break
        px = -2
        py = -2
    elif (ccx != tx) & (ccy != ty):
        fen.after(20,mov)
    else:
        objArr[objIdx][1] = tx/32
        objArr[objIdx][2] = ty/32
        idle = True

def clique(e):
    global idle, tableau, arrows, poss, objIdx, cx, cy, tx, ty, ccx, ccy, px, py
    if not 'idle' in globals():
        idle = True
    if idle:
        x = int(e.x/32)
        y = int(e.y/32)
        action = True
        if not 'arrows' in globals():
            arrows = ['']*0
        if not 'poss' in globals():
            poss = ['']*0
        for i in range(0,len(poss)):
            if (poss[i][0] == x) & (poss[i][1] == y):
                tx = 32*x
                ty = 32*y
                tableau[y] = tableau[y][:x] + tableau[cy][cx] + tableau[y][x+1:]
                tableau[cy] = tableau[cy][:cx] + 'A' + tableau[cy][cx+1:]
                px = poss[i][2]
                py = poss[i][3]
                if (px != -1) & (py != -1):
                    tableau[py] = tableau[py][:px] + 'A' + tableau[py][px+1:]
                    px *= 32
                    py *= 32
                ccx = -1
                ccy = -1
                mov()
                action = False
        if action == False:
            for i in range(0,len(arrows)):
                zone_dessin.delete(arrows[i])
            arrows = ['']*0
            poss = ['']*0
        if (tableau[y][x] == 'E') & (action):
            for i in range(0,len(arrows)):
                zone_dessin.delete(arrows[i])
            arrows = ['']*0
            poss = ['']*0
            for i in range(0,len(objArr)):
                if (objArr[i][1] == x) & (objArr[i][2] == y):
                    cx = x
                    cy = y
                    objIdx = i
                    break
            if tableau[y-1][x+1] in 'AC':
                if tableau[y-1][x+1] == 'A':
                    arrows.append(zone_dessin.create_line(x*32+28,y*32+4,(x+1.5)*32,(y-0.5)*32,fill="ivory",width=5,arrow=LAST))
                    poss.append([x+1,y-1,-1,-1])
                elif (tableau[y-1][x+1] == 'C') & (tableau[y-2][x+2] == "A"):
                    arrows.append(zone_dessin.create_line(x*32+28,y*32+4,(x+2.5)*32,(y-1.5)*32,fill="ivory",width=5,arrow=LAST))
                    poss.append([x+2,y-2,x+1,y-1])
            if tableau[y-1][x-1] in 'AC':
                if tableau[y-1][x-1] == 'A':
                    arrows.append(zone_dessin.create_line(x*32+4,y*32+4,(x-0.5)*32,(y-0.5)*32,fill="ivory",width=5,arrow=LAST))
                    poss.append([x-1,y-1,-1,-1])
                elif (tableau[y-1][x-1] == 'C') & (tableau[y-2][x-2] == "A"):
                    arrows.append(zone_dessin.create_line(x*32+4,y*32+4,(x-1.5)*32,(y-1.5)*32,fill="ivory",width=5,arrow=LAST))
                    poss.append([x-2,y-2,x-1,y-1])
            action = False
        if action:
            for i in range(0,len(arrows)):
                zone_dessin.delete(arrows[i])
            arrows = ['']*0
            poss = ['']*0

def IA():
    done = False
    for i in range(0,len(objArr)):
        if (objArr[i][3] == True):
            x = objArr[i][1]
            y = objArr[i][2]
            if (tableau[y+1][x-1] == 'E') & (tableau[y+2][x-2] == "A"):
                tx = 32*(x-2)
                ty = 32*(y+2)
                tableau[y+2] = tableau[y+2][:x-2] + tableau[y+2][x-2] + tableau[y+2][x-1:]
                tableau[y] = tableau[y][:x] + 'A' + tableau[y][x+1:] #!!!!
                px = poss[i][2]
                py = poss[i][3]
                if (px != -1) & (py != -1):
                    tableau[py] = tableau[py][:px] + 'A' + tableau[py][px+1:]
                    px *= 32
                    py *= 32
                ccx = -1
                ccy = -1
                mov()
            break

def genTab():
    global tableau, objArr
    tableau = ['RRRRRRRRRRRR',
               'RBCBCBCBCBCR',
               'RCBCBCBCBCBR',
               'RBCBCBCBCBCR',
               'RCBCBCBCBCBR',
               'RBABABABABAR',
               'RABABABABABR',
               'RBEBEBEBEBER',
               'REBEBEBEBEBR',
               'RBEBEBEBEBER',
               'REBEBEBEBEBR',
               'RRRRRRRRRRRR']
    for i in range(1,11):
        for j in range(1,11):
            if tableau[j][i] == 'A':
                zone_dessin.create_rectangle(i*32,j*32,i*32+32,j*32+32,fill="tan4")
            elif tableau[j][i] == 'B':
                zone_dessin.create_rectangle(i*32,j*32,i*32+32,j*32+32,fill="wheat1")
            elif tableau[j][i] == 'C':
                zone_dessin.create_rectangle(i*32,j*32,i*32+32,j*32+32,fill="tan4")
            elif tableau[j][i] == 'E':
                zone_dessin.create_rectangle(i*32,j*32,i*32+32,j*32+32,fill="tan4")
    objArr = ['']*0
    for i in range(1,11):
        for j in range(1,11):
            if tableau[j][i] == 'C':
                objArr.append([zone_dessin.create_oval(i*32+4,j*32+4,i*32+28,j*32+28,fill="grey25",outline="grey25"),i,j,False])
            elif tableau[j][i] == 'E':
                objArr.append([zone_dessin.create_oval(i*32+4,j*32+4,i*32+28,j*32+28,fill="lemon chiffon",outline="lemon chiffon"),i,j,True])

fen=Tk()
fen.geometry('400x420')
fen.title("Dames - G-Plateforme")
zone_dessin=Canvas(fen,width=384,height=372)
zone_dessin.pack()
btnRecommencer=Button(fen,text="Rejouer",command=genTab)
btnRecommencer.pack()
zone_dessin.bind("<Button-1>", clique)

genTab()

#frame.pack()

fen.mainloop()

def stopDames(fen):
    global task
    try:
        fen.after_cancel(task)
    except NameError:
        dump = True
