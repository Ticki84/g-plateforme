# Le jeu d'horreur du prolo
from random import randint
def startHorreur():
  print("Le jeu d'horreur du prolo")
  courage=True
  score=0
  while courage:
    porte_fantome=randint(1,7)
    print('7 portes devant toi...')
    print('un fantome est derriere toi.')
    print('quelle porte ouvres-tu ?')
    porte=input('1, 2, 3, 4, 5, 6 ou 7?')
    porte_num=float(porte)
    if porte_num==porte_fantome:
      print('BOUUU')
      courage=False
    else:
      print('Aucun fantôme en vue')
      print('tu ouvres la prochaine porte')
      score=score+1
  print('Fuiiiis')
  print('Game over! ton score est', score)
