from tkinter import*
from random import*

def startPacman(fen, canevas, fond, txtRetour, version):
    def genTab():
        global imgsArr, tableau, score, highscore, pacman, blinky, pinky, inky, clyde
        tableau = ['0111111111111231111111111114',
                   '5RRRRRRRRRRRR67RRRRRRRRRRRR5',
                   '5R899AR8999AR67R8999AR899AR5',
                   '5S6BB7R6BBB7R67R6BBB7R6BB7S5',
                   '5RCDDERCDDDERCERCDDDERCDDER5',
                   '5RRRRRRRRRRRRRRRRRRRRRRRRRR5',
                   '5R899AR8AR8999999AR8AR899AR5',
                   '5RCDDER67RCDDFGDDER67RCDDER5',
                   '5RRRRRR67RRRR67RRRR67RRRRRR5',
                   'H11114R6I99AZ67Z899J7R01111K',
                   'ZZZZZ5R6GDDEZCEZCDDF7R5ZZZZZ',
                   'ZZZZZ5R67ZZZZZZZZZZ67R5ZZZZZ',
                   'ZZZZZ5R67Z011LM114Z67R5ZZZZZ',
                   '11111KRCEZ5ZZZZZZ5ZCERH11111',
                   'ZZZZZZRZZZ5ZZZZZZ5ZZZRZZZZZZ',
                   '111114R8AZ5ZZZZZZ5Z8AR011111',
                   'ZZZZZ5R67ZH111111KZ67R5ZZZZZ',
                   'ZZZZZ5R67ZZZZZZZZZZ67R5ZZZZZ',
                   'ZZZZZ5R67Z8999999AZ67R5ZZZZZ',
                   '01111KRCEZCDDFGDDEZCERH11114',
                   '5RRRRRRRRRRRR67RRRRRRRRRRRR5',
                   '5R899AR8999AR67R8999AR899AR5',
                   '5SCDF7RCDDDERCERCDDDER6GDES5',
                   '5RRR67RRRRRRRZZRRRRRRR67RRR5',
                   'N9AR67R8AR8999999AR8AR67R89O',
                   'PDERCER67RCDDFGDDER67RCERCDQ',
                   '5RRRRRR67RRRR67RRRR67RRRRRR5',
                   '5R89999JI99AR67R899JI9999AR5',
                   '5RCDDDDDDDDERCERCDDDDDDDDER5',
                   '5RRRRRRRRRRRRRRRRRRRRRRRRRR5',
                   'H11111111111111111111111111K']

        liste = canevas.find_all()
        for i in range(0,len(liste)):
            if (liste[i] != txtRetour) & (liste[i] != fond):
                canevas.delete(liste[i])
        imgsArr = ['']*28
        for i in range(0,28):
            imgsArr[i] = ['']*31
        #canevas.create_rectangle(0,0,1200/32*version,version*31, fill="black")
        for i in range(0,31):
            for j in range(0,28):
                if tableau[i][j] in '0123456789ABCDEFGHIJKLMNOPQRS':
                    imgsArr[j][i] = canevas.create_image(152/32*version+j*version,i*version, image=allImgs['0123456789ABCDEFGHIJKLMNOPQRS'.index(tableau[i][j])], anchor=NW)
        canevas.create_text(76/32*version,50/32*version, text="GAME", fill="pink", font=nFont)
        canevas.create_text(76/32*version,80/32*version, text="SCORE", fill="pink", font=nFont)
        score = ['']*6
        score[0] = 0
        score[1] = canevas.create_text(76/32*version,130/32*version, text="0", fill="white", font=nFont)
        score[2] = canevas.create_text(76/32*version,165/32*version, text="0", fill="white", font=nFont)
        score[3] = canevas.create_text(76/32*version,200/32*version, text="0", fill="white", font=nFont)
        score[4] = canevas.create_text(76/32*version,235/32*version, text="0", fill="white", font=nFont)
        score[5] = canevas.create_text(76/32*version,270/32*version, text="0", fill="white", font=nFont)
        btnRejouer = canevas.create_text(76/32*version,560/32*version, text="REJOUER", fill="pink", activefill="white", font=nFont)
        canevas.tag_bind(btnRejouer, '<Button-1>', rejouer)
        #txtRetour = canevas.create_text(1124/32*version,560/32*version, text="RETOUR", fill="pink", activefill="white", font=nFont)
        #canevas.tag_bind(txtRetour, '<Button-1>', stopPacman)
        canevas.create_text(1124/32*version,50/32*version, text="HIGH", fill="pink", font=nFont)
        canevas.create_text(1124/32*version,80/32*version, text="SCORE", fill="pink", font=nFont)
        if not 'highscore' in globals():
            highscore = ['']*6
        if highscore[0] == '':
            highscore[0] = 0
        tscore = str(highscore[0]).zfill(5)
        highscore[1] = canevas.create_text(1124/32*version,130/32*version, text=tscore[0], fill="white", font=nFont)
        highscore[2] = canevas.create_text(1124/32*version,165/32*version, text=tscore[1], fill="white", font=nFont)
        highscore[3] = canevas.create_text(1124/32*version,200/32*version, text=tscore[2], fill="white", font=nFont)
        highscore[4] = canevas.create_text(1124/32*version,235/32*version, text=tscore[3], fill="white", font=nFont)
        highscore[5] = canevas.create_text(1124/32*version,270/32*version, text=tscore[4], fill="white", font=nFont)
        #canevas.create_image(1124,900, image=allImgs[30])
        pacman = [canevas.create_image(152/32*version+13.5*version,23*version,image=allImgs[31], anchor=NW),13.5,23.0,0,0,0,31,0]
        blinky = [canevas.create_image(152/32*version+13.5*version,11*version,image=allImgs[43], anchor=NW),13.5,11.0,2,0,43,-1,0,False]
        pinky = [canevas.create_image(152/32*version+13.5*version,14*version,image=allImgs[57], anchor=NW),13.5,14.0,1,0,57,0,60,False]
        inky = [canevas.create_image(152/32*version+11.5*version,14*version,image=allImgs[69], anchor=NW),11.5,14.0,3,0,69,0,120,False]
        clyde = [canevas.create_image(152/32*version+15.5*version,14*version,image=allImgs[53], anchor=NW),15.5,14.0,3,0,53,0,240,False]
        task[0] = -1

    def tar(x, y, ori):
        x = int(x)
        y = int(y)
        if ori == 0:
            while tableau[y][x] not in 'RSYZ':
                x += 1
                if x > 26:
                    break
            return x
        elif ori == 1:
            while tableau[y][x] not in 'RSYZ':
                y += 1
            return y
        elif ori == 2:
            while tableau[y][x] not in 'RSYZ':
                x -= 1
                if x < 1:
                    break
            return x
        elif ori == 3:
            while tableau[y][x] not in 'RSYZ':
                y -= 1
            return y

    def getPath(x, y, ori):
        c = [0,0,0,0,0]
        if (tableau[int(y)][int(x)+1] in 'RSYZ') & (ori != 2) & (y%1 == 0):
            c[4] += 1
            c[0] = True
        if (tableau[int(y)+1][int(x)] in 'RSYZ') & (ori != 3) & (x%1 == 0):
            c[4] += 1
            c[1] = True
        if (tableau[int(y)][int(x)-1] in 'RSYZ') & (ori != 0) & (y%1 == 0):
            c[4] += 1
            c[2] = True
        if (tableau[int(y)-1][int(x)] in 'RSYZ') & (ori != 1) & (x%1 == 0):
            c[4] += 1
            c[3] = True
        return c

    def toLoc(x, y, tarx, tary, ori, inv):
        c = getPath(x, y, ori)
        if c[4] == 1:
            return c.index(True)
        else:
            if inv:
                tarx = 27 - tarx
                tary = 30 - tary
            if x > tarx:
                dx = x - tarx
            elif x < tarx:
                dx = tarx - x
            else:
                dx = 0
            if y > tary:
                dy = y - tary
            elif y < tary:
                dy = tary - y
            else:
                dy = 0
                
            if dx > dy:
                if tarx > x:
                    if c[0] == True:
                        return 0
                    elif tary > y:
                        if c[1] == True:
                            return 1
                        elif c[3] == True:
                            return 3
                        else:
                            return 2
                    elif tary < y:
                        if c[3] == True:
                            return 3
                        elif c[1] == True:
                            return 1
                        else:
                            return 2
                    else:
                        if (c[1] == True) & (c[3] == True):
                            return randint(0,1)*2+1
                        elif c[1] == True:
                            return 1
                        elif c[3] == True:
                            return 3
                        else:
                            return 2
                elif tarx < x:
                    if c[2] == True:
                        return 2
                    elif tary > y:
                        if c[1] == True:
                            return 1
                        elif c[3] == True:
                            return 3
                        else:
                            return 0
                    elif tary < y:
                        if c[3] == True:
                            return 3
                        elif c[1] == True:
                            return 1
                        else:
                            return 0
                    else:
                        if (c[1] == True) & (c[3] == True):
                            return randint(0,1)*2+1
                        elif c[1] == True:
                            return 1
                        elif c[3] == True:
                            return 3
                        else:
                            return 0
                else:
                    if tary > y:
                        if c[1] == True:
                            return 1
                        elif (c[0] == True) & (c[2] == True):
                            return randint(0,1)*2
                        elif c[0] == True:
                            return 0
                        elif c[2] == True:
                            return 2
                        else:
                            return 3
                    elif tary < y:
                        if c[3] == True:
                            return 3
                        elif (c[0] == True) & (c[2] == True):
                            return randint(0,1)*2
                        elif c[0] == True:
                            return 0
                        elif c[2] == True:
                            return 2
                        else:
                            return 1
                    else:
                        rint = randint(0,3)
                        while c[rint] != True:
                            rint = randint(0,3)
                        return rint
            elif dy > dx:
                if tary > y:
                    if c[1] == True:
                        return 1
                    elif tarx > x:
                        if c[0] == True:
                            return 0
                        elif c[2] == True:
                            return 2
                        else:
                            return 3
                    elif tarx < x:
                        if c[2] == True:
                            return 2
                        elif c[0] == True:
                            return 0
                        else:
                            return 3
                    else:
                        if (c[0] == True) & (c[2] == True):
                            return randint(0,1)*2
                        elif c[0] == True:
                            return 0
                        elif c[2] == True:
                            return 2
                        else:
                            return 3
                elif tary < y:
                    if c[3] == True:
                        return 3
                    elif tarx > x:
                        if c[0] == True:
                            return 0
                        elif c[2] == True:
                            return 2
                        else:
                            return 1
                    elif tarx < x:
                        if c[2] == True:
                            return 2
                        elif c[0] == True:
                            return 0
                        else:
                            return 1
                    else:
                        if (c[0] == True) & (c[2] == True):
                            return randint(0,1)*2
                        elif c[0] == True:
                            return 0
                        elif c[2] == True:
                            return 2
                        else:
                            return 3
                else:
                    if tarx > x:
                        if c[0] == True:
                            return 0
                        elif (c[1] == True) & (c[3] == True):
                            return randint(0,1)*2+1
                        elif c[1] == True:
                            return 1
                        elif c[3] == True:
                            return 3
                        else:
                            return 2
                    elif tarx < x:
                        if c[2] == True:
                            return 2
                        elif (c[1] == True) & (c[3] == True):
                            return randint(0,1)*2+1
                        elif c[1] == True:
                            return 1
                        elif c[3] == True:
                            return 3
                        else:
                            return 0
                    else:
                        rint = randint(0,3)
                        while c[rint] != True:
                            rint = randint(0,3)
                        return rint
            else:
                rint = randint(0,3)
                while c[rint] != True:
                    rint = randint(0,3)
                return rint

    def collision():
        global tableau, pacman, blinky, pinky, inky, clyde, nbManger

        def refScore():
            tscore = str(score[0]).zfill(5)
            canevas.delete(score[1])
            score[1] = canevas.create_text(76/32*version,130/32*version, text=tscore[0], fill="white", font=nFont)
            canevas.delete(score[2])
            score[2] = canevas.create_text(76/32*version,165/32*version, text=tscore[1], fill="white", font=nFont)
            canevas.delete(score[3])
            score[3] = canevas.create_text(76/32*version,200/32*version, text=tscore[2], fill="white", font=nFont)
            canevas.delete(score[4])
            score[4] = canevas.create_text(76/32*version,235/32*version, text=tscore[3], fill="white", font=nFont)
            canevas.delete(score[5])
            score[5] = canevas.create_text(76/32*version,270/32*version, text=tscore[4], fill="white", font=nFont)
            if score[0] > highscore[0]:
                highscore[0] = score[0]
                canevas.delete(highscore[1])
                highscore[1] = canevas.create_text(1124/32*version,130/32*version, text=tscore[0], fill="white", font=nFont)
                canevas.delete(highscore[2])
                highscore[2] = canevas.create_text(1124/32*version,165/32*version, text=tscore[1], fill="white", font=nFont)
                canevas.delete(highscore[3])
                highscore[3] = canevas.create_text(1124/32*version,200/32*version, text=tscore[2], fill="white", font=nFont)
                canevas.delete(highscore[4])
                highscore[4] = canevas.create_text(1124/32*version,235/32*version, text=tscore[3], fill="white", font=nFont)
                canevas.delete(highscore[5])
                highscore[5] = canevas.create_text(1124/32*version,270/32*version, text=tscore[4], fill="white", font=nFont)

        if (pacman[1]%1 == 0) & (pacman[2]%1 == 0) & (tableau[int(pacman[2])][int(pacman[1])] == 'R'):
            tableau[int(pacman[2])] = tableau[int(pacman[2])][:int(pacman[1])] + 'Y' + tableau[int(pacman[2])][int(pacman[1])+1:]
            canevas.delete(imgsArr[int(pacman[1])][int(pacman[2])])
            score[0] += 10
            refScore()

        if (pacman[1]%1 == 0) & (pacman[2]%1 == 0) & (tableau[int(pacman[2])][int(pacman[1])] == 'S'):
            pacman[7] += 500
            tableau[int(pacman[2])] = tableau[int(pacman[2])][:int(pacman[1])] + 'Y' + tableau[int(pacman[2])][int(pacman[1])+1:]
            canevas.delete(imgsArr[int(pacman[1])][int(pacman[2])])
            blinky[8] = True
            pinky[8] = True
            inky[8] = True
            clyde[8] = True
            if blinky[6] == -1:
                blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], -1, True)
            if pinky[6] == -1:
                pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], pacman[2], -1, True)
            if inky[6] == -1:
                inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], -1, True)
            if clyde[6] == -1:
                clyde[3] = toLoc(clyde[1], clyde[2], pacman[1], pacman[2], -1, True)

        if (pacman[1] == blinky[1]) & (pacman[2] == blinky[2]):
            if (pacman[7] > 0) & (blinky[8] == True):
                blinky[6] = -2
                score[0] += 200*(2**nbManger)
                refScore()
                nbManger += 1
                blinky[8] = False
            elif (blinky[6] > -2) | (blinky[6] == -4):
                rejouer(None)
        if (pacman[1] == pinky[1]) & (pacman[2] == pinky[2]):
            if (pacman[7] > 0) & (pinky[8] == True):
                pinky[6] = -2
                score[0] += 200*(2**nbManger)
                refScore()
                nbManger += 1
                pinky[8] = False
            elif (pinky[6] > -2) | (pinky[6] == -4):
                rejouer(None)
        if (pacman[1] == inky[1]) & (pacman[2] == inky[2]):
            if (pacman[7] > 0) & (inky[8] == True):
                inky[6] = -2
                score[0] += 200*(2**nbManger)
                refScore()
                nbManger += 1
                inky[8] = False
            elif (inky[6] > -2) | (inky[6] == -4):
                rejouer(None)
        if (pacman[1] == clyde[1]) & (pacman[2] == clyde[2]):
            if (pacman[7] > 0) & (clyde[8] == True):
                clyde[6] = -2
                score[0] += 200*(2**nbManger)
                refScore()
                nbManger += 1
                clyde[8] = False
            elif (clyde[6] > -2) | (clyde[6] == -4):
                rejouer(None)


    def mPacman():
        global task, tableau, pacman, blinky, pinky, inky, clyde, nbManger
        
        if (tableau[int(pacman[2])][int(pacman[1])+1] in 'RSYZ') & (pacman[4] == 0) & (pacman[2]%1 == 0):
            pacman[3] = 0
        elif (tableau[int(pacman[2])+1][int(pacman[1])] in 'RSYZ') & (pacman[4] == 1) & (pacman[1]%1 == 0):
            pacman[3] = 1
        elif (tableau[int(pacman[2])][int(pacman[1])-1] in 'RSYZ') & (pacman[4] == 2) & (pacman[2]%1 == 0):
            pacman[3] = 2
        elif (tableau[int(pacman[2])-1][int(pacman[1])] in 'RSYZ') & (pacman[4] == 3) & (pacman[1]%1 == 0):
            pacman[3] = 3
        if (pacman[3] == 0) & (tableau[int(pacman[2])][int(pacman[1])+1] in 'RSYZ'):
            pacman[6] = 31
            pacman[1] = float("%.1f" %(pacman[1] + 0.1))
            pacman[5] += 1
        elif (pacman[3] == 1) & (tableau[int(pacman[2])+1][int(pacman[1])] in 'RSYZ'):
            pacman[6] = 33
            pacman[2] = float("%.1f" %(pacman[2] + 0.1))
            pacman[5] += 1
        elif (pacman[3] == 2) & (tableau[int(pacman[2])][int(pacman[1]-0.1)] in 'RSYZ'):
            pacman[6] = 35
            pacman[1] = float("%.1f" %(pacman[1] - 0.1))
            pacman[5] += 1
        elif (pacman[3] == 3) & (tableau[int(pacman[2]-0.1)][int(pacman[1])] in 'RSYZ'):
            pacman[6] = 37
            pacman[2] = float("%.1f" %(pacman[2] - 0.1))
            pacman[5] += 1
        if pacman[2] == 14.0:
            if (pacman[1] > 26.9) & (pacman[3] == 0):
                pacman[1] = 0.1
            elif (pacman[1] < 0.1) & (pacman[3] == 2):
                pacman[1] = 26.9
        collision()
        canevas.delete(pacman[0])
        if pacman[5] < 5:
            pacman[0] = canevas.create_image(152/32*version+pacman[1]*version,pacman[2]*version,image=allImgs[pacman[6]], anchor=NW)
        elif pacman[5] > 4:
            pacman[0] = canevas.create_image(152/32*version+pacman[1]*version,pacman[2]*version,image=allImgs[pacman[6]+1], anchor=NW)
            if pacman[5] == 9:
                pacman[5] = 0
        if pacman[7] > 0:
            pacman[7] -= 1
        else:
            nbManger = 0
            blinky[8] = False
            pinky[8] = False
            inky[8] = False
            clyde[8] = False
        if task[0] != -1:
            task[1] = fen.after(30, mPacman)

    def mBlinky():
        global task, tableau, pacman, blinky, pinky, inky, clyde

        if (blinky[1]%1 == 0) & (blinky[2]%1 == 0):
            if (blinky[3] == 0) & ((tableau[int(blinky[2])+1][int(blinky[1])] in 'RSYZ') | (tableau[int(blinky[2])][int(blinky[1])+1] in 'RSYZ') | (tableau[int(blinky[2])-1][int(blinky[1])] in 'RSYZ')):
                blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 0, False)
                if (pacman[7] > 0) & (blinky[8] == True):
                    blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 0, True)
                if blinky[6] == -2:
                    blinky[3] = toLoc(blinky[1], blinky[2], 13.5, 11, 0, False)
            elif (blinky[3] == 1) & ((tableau[int(blinky[2])][int(blinky[1])+1] in 'RSYZ') | (tableau[int(blinky[2])][int(blinky[1])-1] in 'RSYZ') | (tableau[int(blinky[2])+1][int(blinky[1])] in 'RSYZ')):
                blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 1, False)
                if (pacman[7] > 0) & (blinky[8] == True):
                    blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 1, True)
                if blinky[6] == -2:
                    blinky[3] = toLoc(blinky[1], blinky[2], 13.5, 11, 1, False)
            elif (blinky[3] == 2) & ((tableau[int(blinky[2])+1][int(blinky[1])] in 'RSYZ') | (tableau[int(blinky[2])][int(blinky[1])-1] in 'RSYZ') | (tableau[int(blinky[2])-1][int(blinky[1])] in 'RSYZ')):
                blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 2, False)
                if (pacman[7] > 0) & (blinky[8] == True):
                    blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 2, True)
                if blinky[6] == -2:
                    blinky[3] = toLoc(blinky[1], blinky[2], 13.5, 11, 2, False)
            elif (blinky[3] == 3) & ((tableau[int(blinky[2])][int(blinky[1])+1] in 'RSYZ') | (tableau[int(blinky[2])][int(blinky[1])-1] in 'RSYZ') | (tableau[int(blinky[2])-1][int(blinky[1])] in 'RSYZ')):
                blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 3, False)
                if (pacman[7] > 0) & (blinky[8] == True):
                    blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 3, True)
                if blinky[6] == -2:
                    blinky[3] = toLoc(blinky[1], blinky[2], 13.5, 11, 3, False)
        if (blinky[2]%1 == 0) & (blinky[6] < -1):
            if (blinky[1] == 13.5) & (blinky[2] == 11) & (blinky[6] == -2):
                blinky[3] = 1
                blinky[6] = -3
            if (blinky[1] == 13.5) & (blinky[2] == 14) & (blinky[6] == -3):
                blinky[3] = 3
                blinky[6] = -4
            if (blinky[1] == 13.5) & (blinky[2] == 11) & (blinky[6] == -4):
                blinky[3] = toLoc(blinky[1], blinky[2], pacman[1], pacman[2], 3, False)
                blinky[6] = -1
        if blinky[3] == 0:
            blinky[5] = 39
            blinky[1] = float("%.1f" %(blinky[1] + 0.1))
        elif blinky[3] == 1:
            blinky[5] = 41
            blinky[2] = float("%.1f" %(blinky[2] + 0.1))
        elif blinky[3] == 2:
            blinky[5] = 43
            blinky[1] = float("%.1f" %(blinky[1] - 0.1))
        elif blinky[3] == 3:
            blinky[5] = 45
            blinky[2] = float("%.1f" %(blinky[2] - 0.1))
        if blinky[2] == 14.0:
            if (blinky[1] > 26.9) & (blinky[3] == 0):
                blinky[1] = 0.1
            elif (blinky[1] < 0.1) & (blinky[3] == 2):
                blinky[1] = 26.9
        blinky[4] += 1
        if (pacman[7] > 50) & (blinky[8] == True):
            blinky[5] = 71
        elif (pacman[7] > 0) & (blinky[8] == True):
            if 40 < pacman[7] < 51:
                blinky[5] = 73
            elif 30 < pacman[7] < 41:
                blinky[5] = 71
            elif 20 < pacman[7] < 31:
                blinky[5] = 73
            elif 10 < pacman[7] < 21:
                blinky[5] = 71
            elif 0 < pacman[7] < 11:
                blinky[5] = 73
        if -4 < blinky[6] < -1:
            blinky[5] = 75
        collision()
        canevas.delete(blinky[0])
        if blinky[4] < 5:
            blinky[0] = canevas.create_image(152/32*version+blinky[1]*version,blinky[2]*version,image=allImgs[blinky[5]], anchor=NW)
        elif blinky[4] > 4:
            blinky[0] = canevas.create_image(152/32*version+blinky[1]*version,blinky[2]*version,image=allImgs[blinky[5]+1], anchor=NW)
            if blinky[4] == 9:
                blinky[4] = 0
        if blinky[7] > 0:
            blinky[7] -= 1
        if (blinky[7] == 0) & (blinky[6] != -1) & (blinky[6] > -2):
            blinky[6] = 1
        if task[0] != -1:
            if (pacman[7] > 0) & (blinky[8] == True):
                task[5] = fen.after(42, mBlinky)
            elif -4 < blinky[6] < -1:
                task[5] = fen.after(15, mBlinky)
            else:
                task[5] = fen.after(30, mBlinky)

    def mPinky():
        global task, tableau, pacman, blinky, pinky, inky, clyde

        if (pinky[1]%1 == 0) & (pinky[2]%1 == 0):
            if (pinky[3] == 0) & ((tableau[int(pinky[2])+1][int(pinky[1])] in 'RSYZ') | (tableau[int(pinky[2])][int(pinky[1])+1] in 'RSYZ') | (tableau[int(pinky[2])-1][int(pinky[1])] in 'RSYZ')):
                if pacman[3] == 0:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],0), pacman[2], 0, False)
                elif pacman[3] == 1:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],1), 0, False)
                elif pacman[3] == 2:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],3), pacman[2], 0, False)
                elif pacman[3] == 3:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],3), 0, False)
                if (pacman[7] > 0) & (pinky[8] == True):
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], pacman[2], 0, True)
                if pinky[6] == -2:
                    pinky[3] = toLoc(pinky[1], pinky[2], 13.5, 11, 0, False)
            elif (pinky[3] == 1) & ((tableau[int(pinky[2])][int(pinky[1])+1] in 'RSYZ') | (tableau[int(pinky[2])][int(pinky[1])-1] in 'RSYZ') | (tableau[int(pinky[2])+1][int(pinky[1])] in 'RSYZ')):
                if pacman[3] == 0:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],0), pacman[2], 1, False)
                elif pacman[3] == 1:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],1), 1, False)
                elif pacman[3] == 2:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],2), pacman[2], 1, False)
                elif pacman[3] == 3:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],3), 1, False)
                if (pacman[7] > 0) & (pinky[8] == True):
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], pacman[2], 1, True)
                if pinky[6] == -2:
                    pinky[3] = toLoc(pinky[1], pinky[2], 13.5, 11, 1, False)
            elif (pinky[3] == 2) & ((tableau[int(pinky[2])+1][int(pinky[1])] in 'RSYZ') | (tableau[int(pinky[2])][int(pinky[1])-1] in 'RSYZ') | (tableau[int(pinky[2])-1][int(pinky[1])] in 'RSYZ')):
                if pacman[3] == 0:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],0), pacman[2], 2, False)
                elif pacman[3] == 1:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],1), 2, False)
                elif pacman[3] == 2:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],2), pacman[2], 2, False)
                elif pacman[3] == 3:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],3), 2, False)
                if (pacman[7] > 0) & (pinky[8] == True):
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], pacman[2], 2, True)
                if pinky[6] == -2:
                    pinky[3] = toLoc(pinky[1], pinky[2], 13.5, 11, 2, False)
            elif (pinky[3] == 3) & ((tableau[int(pinky[2])][int(pinky[1])+1] in 'RSYZ') | (tableau[int(pinky[2])][int(pinky[1])-1] in 'RSYZ') | (tableau[int(pinky[2])-1][int(pinky[1])] in 'RSYZ')):
                if pacman[3] == 0:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],0), pacman[2], 3, False)
                elif pacman[3] == 1:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],1), 3, False)
                elif pacman[3] == 2:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],2), pacman[2], 3, False)
                elif pacman[3] == 3:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],3), 3, False)
                if (pacman[7] > 0) & (pinky[8] == True):
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], pacman[2], 3, True)
                if pinky[6] == -2:
                    pinky[3] = toLoc(pinky[1], pinky[2], 13.5, 11, 3, False)
        elif pinky[6] == 0:
            if pinky[2]%1 == 0:
                if pinky[3] == 1:
                    if tableau[int(pinky[2])+1][int(pinky[1])] == 'Z':
                        pinky[3] = 1
                    else:
                        pinky[3] = 3
                else:
                    if tableau[int(pinky[2])-1][int(pinky[1])] == 'Z':
                        pinky[3] = 3
                    else:
                        pinky[3] = 1
        elif pinky[6] == 1:
            if pinky[2]%1 == 0:
                if pinky[3] == 1:
                    if tableau[int(pinky[2])+1][int(pinky[1])] == 'Z':
                        pinky[3] = 1
                    else:
                        pinky[3] = 3
                else:
                    if pinky[2] == 11.0:
                        if pacman[1] > pinky[1]:
                            pinky[3] = 0
                        elif pacman[1] < pinky[1]:
                            pinky[3] = 2
                        else:
                            pinky[3] = randint(0,1)*2
                        pinky[6] = -1
                    else:
                        pinky[3] = 3
        if (pinky[2]%1 == 0) & (pinky[6] < -1):
            if (pinky[1] == 13.5) & (pinky[2] == 11) & (pinky[6] == -2):
                pinky[3] = 1
                pinky[6] = -3
            if (pinky[1] == 13.5) & (pinky[2] == 14) & (pinky[6] == -3):
                pinky[3] = 3
                pinky[6] = -4
            if (pinky[1] == 13.5) & (pinky[2] == 11) & (pinky[6] == -4):
                if pacman[3] == 0:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],0), pacman[2], 3, False)
                elif pacman[3] == 1:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],1), 3, False)
                elif pacman[3] == 2:
                    pinky[3] = toLoc(pinky[1], pinky[2], tar(pacman[1],pacman[2],2), pacman[2], 3, False)
                elif pacman[3] == 3:
                    pinky[3] = toLoc(pinky[1], pinky[2], pacman[1], tar(pacman[1],pacman[2],3), 3, False)
                pinky[6] = -1
        if pinky[3] == 0:
            pinky[5] = 55
            pinky[1] = float("%.1f" %(pinky[1] + 0.1))
        elif pinky[3] == 1:
            pinky[5] = 57
            pinky[2] = float("%.1f" %(pinky[2] + 0.1))
        elif pinky[3] == 2:
            pinky[5] = 59
            pinky[1] = float("%.1f" %(pinky[1] - 0.1))
        elif pinky[3] == 3:
            pinky[5] = 61
            pinky[2] = float("%.1f" %(pinky[2] - 0.1))
        if pinky[2] == 14.0:
            if (pinky[1] > 26.9) & (pinky[3] == 0):
                pinky[1] = 0.1
            elif (pinky[1] < 0.1) & (pinky[3] == 2):
                pinky[1] = 26.9
        pinky[4] += 1
        if (pacman[7] > 50) & (pinky[8] == True):
            pinky[5] = 71
        elif (pacman[7] > 0) & (pinky[8] == True):
            if 40 < pacman[7] < 51:
                pinky[5] = 73
            elif 30 < pacman[7] < 41:
                pinky[5] = 71
            elif 20 < pacman[7] < 31:
                pinky[5] = 73
            elif 10 < pacman[7] < 21:
                pinky[5] = 71
            elif 0 < pacman[7] < 11:
                pinky[5] = 73
        if -4 < pinky[6] < -1:
            pinky[5] = 75
        collision()
        canevas.delete(pinky[0])
        if pinky[4] < 5:
            pinky[0] = canevas.create_image(152/32*version+pinky[1]*version,pinky[2]*version,image=allImgs[pinky[5]], anchor=NW)
        elif pinky[4] > 4:
            pinky[0] = canevas.create_image(152/32*version+pinky[1]*version,pinky[2]*version,image=allImgs[pinky[5]+1], anchor=NW)
            if pinky[4] == 9:
                pinky[4] = 0
        if pinky[7] > 0:
            pinky[7] -= 1
        if (pinky[7] == 0) & (pinky[6] != -1) & (pinky[6] > -2):
            pinky[6] = 1
        if task[0] != -1:
            if (pacman[7] > 0) & (pinky[8] == True):
                task[5] = fen.after(42, mPinky)
            elif -4 < pinky[6] < -1:
                task[5] = fen.after(15, mPinky)
            else:
                task[5] = fen.after(30, mPinky)

    def mInky():
        global task, tableau, pacman, blinky, pinky, inky, clyde
        
        if (inky[1]%1 == 0) & (inky[2]%1 == 0):
            if (inky[3] == 0) & ((tableau[int(inky[2])+1][int(inky[1])] in 'RSYZ') | (tableau[int(inky[2])][int(inky[1])+1] in 'RSYZ') | (tableau[int(inky[2])-1][int(inky[1])] in 'RSYZ')):
                if randint(0,1) == 0:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 0, False)
                else:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 0, True)
                if (pacman[7] > 0) & (inky[8] == True):
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 0, True)
                if inky[6] == -2:
                    inky[3] = toLoc(inky[1], inky[2], 13.5, 11, 0, False)
            elif (inky[3] == 1) & ((tableau[int(inky[2])][int(inky[1])+1] in 'RSYZ') | (tableau[int(inky[2])][int(inky[1])-1] in 'RSYZ') | (tableau[int(inky[2])+1][int(inky[1])] in 'RSYZ')):
                if randint(0,1) == 0:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 1, False)
                else:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 1, True)
                if (pacman[7] > 0) & (inky[8] == True):
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 1, True)
                if inky[6] == -2:
                    inky[3] = toLoc(inky[1], inky[2], 13.5, 11, 1, False)
            elif (inky[3] == 2) & ((tableau[int(inky[2])+1][int(inky[1])] in 'RSYZ') | (tableau[int(inky[2])][int(inky[1])-1] in 'RSYZ') | (tableau[int(inky[2])-1][int(inky[1])] in 'RSYZ')):
                if randint(0,1) == 0:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 2, False)
                else:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 2, True)
                if (pacman[7] > 0) & (inky[8] == True):
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 2, True)
                if inky[6] == -2:
                    inky[3] = toLoc(inky[1], inky[2], 13.5, 11, 2, False)
            elif (inky[3] == 3) & ((tableau[int(inky[2])][int(inky[1])+1] in 'RSYZ') | (tableau[int(inky[2])][int(inky[1])-1] in 'RSYZ') | (tableau[int(inky[2])-1][int(inky[1])] in 'RSYZ')):
                if randint(0,1) == 0:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 3, False)
                else:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 3, True)
                if (pacman[7] > 0) & (inky[8] == True):
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 3, True)
                if inky[6] == -2:
                    inky[3] = toLoc(inky[1], inky[2], 13.5, 11, 3, False)
        if inky[6] == 0:
            if inky[2]%1 == 0:
                if inky[3] == 1:
                    if tableau[int(inky[2])+1][int(inky[1])] == 'Z':
                        inky[3] = 1
                    else:
                        inky[3] = 3
                else:
                    if tableau[int(inky[2])-1][int(inky[1])] == 'Z':
                        inky[3] = 3
                    else:
                        inky[3] = 1
        elif inky[6] == 1:
            if inky[1] == 13.5:
                inky[3] = 3
                if inky[2] == 11.0:
                    inky[3] = randint(0,1)*2
                    inky[6] = -1
            else:
                inky[3] = 0
        if (inky[2]%1 == 0) & (inky[6] < -1):
            if (inky[1] == 13.5) & (inky[2] == 11) & (inky[6] == -2):
                inky[3] = 1
                inky[6] = -3
            if (inky[1] == 13.5) & (inky[2] == 14) & (inky[6] == -3):
                inky[3] = 3
                inky[6] = -4
            if (inky[1] == 13.5) & (inky[2] == 11) & (inky[6] == -4):
                if randint(0,1) == 0:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 3, False)
                else:
                    inky[3] = toLoc(inky[1], inky[2], pacman[1], pacman[2], 3, True)
                pinky[6] = -1
        if inky[3] == 0:
            inky[5] = 63
            inky[1] = float("%.1f" %(inky[1] + 0.1))
        elif inky[3] == 1:
            inky[5] = 65
            inky[2] = float("%.1f" %(inky[2] + 0.1))
        elif inky[3] == 2:
            inky[5] = 67
            inky[1] = float("%.1f" %(inky[1] - 0.1))
        elif inky[3] == 3:
            inky[5] = 69
            inky[2] = float("%.1f" %(inky[2] - 0.1))
        if inky[2] == 14.0:
            if (inky[1] > 26.9) & (inky[3] == 0):
                inky[1] = 0.1
            elif (inky[1] < 0.1) & (inky[3] == 2):
                inky[1] = 26.9
        inky[4] += 1
        if (pacman[7] > 50) & (inky[8] == True):
            inky[5] = 71
        elif (pacman[7] > 0) & (inky[8] == True):
            if 40 < pacman[7] < 51:
                inky[5] = 73
            elif 30 < pacman[7] < 41:
                inky[5] = 71
            elif 20 < pacman[7] < 31:
                inky[5] = 73
            elif 10 < pacman[7] < 21:
                inky[5] = 71
            elif 0 < pacman[7] < 11:
                inky[5] = 73
        if -4 < inky[6] < -1:
            inky[5] = 75
        collision()
        canevas.delete(inky[0])
        if inky[4] < 5:
            inky[0] = canevas.create_image(152/32*version+inky[1]*version,inky[2]*version,image=allImgs[inky[5]], anchor=NW)
        elif inky[4] > 4:
            inky[0] = canevas.create_image(152/32*version+inky[1]*version,inky[2]*version,image=allImgs[inky[5]+1], anchor=NW)
            if inky[4] == 9:
                inky[4] = 0
        if inky[7] > 0:
            inky[7] -= 1
        if (inky[7] == 0) & (inky[6] != -1) & (inky[6] > -2):
            inky[6] = 1
        if task[0] != -1:
            if (pacman[7] > 0) & (inky[8] == True):
                task[5] = fen.after(42, mInky)
            elif -4 < inky[6] < -1:
                task[5] = fen.after(15, mInky)
            else:
                task[5] = fen.after(30, mInky)
        
    def mClyde():
        global task, tableau, pacman, blinky, pinky, inky, clyde
        
        if (clyde[1]%1 == 0) & (clyde[2]%1 == 0):
            if (clyde[3] == 0) & ((tableau[int(clyde[2])+1][int(clyde[1])] in 'RSYZ') | (tableau[int(clyde[2])][int(clyde[1])+1] in 'RSYZ') | (tableau[int(clyde[2])-1][int(clyde[1])] in 'RSYZ')):
                c = getPath(clyde[1],clyde[2],0)
                if c[4] == 1:
                    clyde[3] = c.index(True)
                else:
                    rint = randint(0,3)
                    while c[rint] != True:
                        rint = randint(0,3)
                    clyde[3] = rint
                    if (pacman[7] > 0) & (clyde[8] == True):
                        clyde[3] = toLoc(clyde[1], clyde[2], pacman[1], pacman[2], 0, True)
                    if clyde[6] == -2:
                        clyde[3] = toLoc(clyde[1], clyde[2], 13.5, 11, 0, False)
            elif (clyde[3] == 1) & ((tableau[int(clyde[2])][int(clyde[1])+1] in 'RSYZ') | (tableau[int(clyde[2])][int(clyde[1])-1] in 'RSYZ') | (tableau[int(clyde[2])+1][int(clyde[1])] in 'RSYZ')):
                c = getPath(clyde[1],clyde[2],1)
                if c[4] == 1:
                    clyde[3] = c.index(True)
                else:
                    rint = randint(0,3)
                    while c[rint] != True:
                        rint = randint(0,3)
                    clyde[3] = rint
                    if (pacman[7] > 0) & (clyde[8] == True):
                        clyde[3] = toLoc(clyde[1], clyde[2], pacman[1], pacman[2], 1, True)
                    if clyde[6] == -2:
                        clyde[3] = toLoc(clyde[1], clyde[2], 13.5, 11, 1, False)
            elif (clyde[3] == 2) & ((tableau[int(clyde[2])+1][int(clyde[1])] in 'RSYZ') | (tableau[int(clyde[2])][int(clyde[1])-1] in 'RSYZ') | (tableau[int(clyde[2])-1][int(clyde[1])] in 'RSYZ')):
                c = getPath(clyde[1],clyde[2],2)
                if c[4] == 1:
                    clyde[3] = c.index(True)
                else:
                    rint = randint(0,3)
                    while c[rint] != True:
                        rint = randint(0,3)
                    clyde[3] = rint
                    if (pacman[7] > 0) & (clyde[8] == True):
                        clyde[3] = toLoc(clyde[1], clyde[2], pacman[1], pacman[2], 2, True)
                    if clyde[6] == -2:
                        clyde[3] = toLoc(clyde[1], clyde[2], 13.5, 11, 2, False)
            elif (clyde[3] == 3) & ((tableau[int(clyde[2])][int(clyde[1])+1] in 'RSYZ') | (tableau[int(clyde[2])][int(clyde[1])-1] in 'RSYZ') | (tableau[int(clyde[2])-1][int(clyde[1])] in 'RSYZ')):
                c = getPath(clyde[1],clyde[2],3)
                if c[4] == 1:
                    clyde[3] = c.index(True)
                else:
                    rint = randint(0,3)
                    while c[rint] != True:
                        rint = randint(0,3)
                    clyde[3] = rint
                    if (pacman[7] > 0) & (clyde[8] == True):
                        clyde[3] = toLoc(clyde[1], clyde[2], pacman[1], pacman[2], 3, True)
                    if clyde[6] == -2:
                        clyde[3] = toLoc(clyde[1], clyde[2], 13.5, 11, 3, False)
        elif clyde[6] == 0:
            if clyde[2]%1 == 0:
                if clyde[3] == 1:
                    if tableau[int(clyde[2])+1][int(clyde[1])] == 'Z':
                        clyde[3] = 1
                    else:
                        clyde[3] = 3
                else:
                    if tableau[int(clyde[2])-1][int(clyde[1])] == 'Z':
                        clyde[3] = 3
                    else:
                        clyde[3] = 1
        elif clyde[6] == 1:
            if clyde[1] == 13.5:
                clyde[3] = 3
                if clyde[2] == 11.0:
                    clyde[3] = randint(0,1)*2
                    clyde[6] = -1
            else:
                clyde[3] = 2
        if (clyde[2]%1 == 0) & (clyde[6] < -1):
            if (clyde[1] == 13.5) & (clyde[2] == 11) & (clyde[6] == -2):
                clyde[3] = 1
                clyde[6] = -3
            if (clyde[1] == 13.5) & (clyde[2] == 14) & (clyde[6] == -3):
                clyde[3] = 3
                clyde[6] = -4
            if (clyde[1] == 13.5) & (clyde[2] == 11) & (clyde[6] == -4):
                clyde[3] = randint(0,1)*2
                pinky[6] = -1
        if clyde[3] == 0:
            clyde[5] = 47
            clyde[1] = float("%.1f" %(clyde[1] + 0.1))
        elif clyde[3] == 1:
            clyde[5] = 49
            clyde[2] = float("%.1f" %(clyde[2] + 0.1))
        elif clyde[3] == 2:
            clyde[5] = 51
            clyde[1] = float("%.1f" %(clyde[1] - 0.1))
        elif clyde[3] == 3:
            clyde[5] = 53
            clyde[2] = float("%.1f" %(clyde[2] - 0.1))
        if clyde[2] == 14.0:
            if (clyde[1] > 26.9) & (clyde[3] == 0):
                clyde[1] = 0.1
            elif (clyde[1] < 0.1) & (clyde[3] == 2):
                clyde[1] = 26.9
        clyde[4] += 1
        if (pacman[7] > 50) & (clyde[8] == True):
            clyde[5] = 71
        elif (pacman[7] > 0) & (clyde[8] == True):
            if 40 < pacman[7] < 51:
                clyde[5] = 73
            elif 30 < pacman[7] < 41:
                clyde[5] = 71
            elif 20 < pacman[7] < 31:
                clyde[5] = 73
            elif 10 < pacman[7] < 21:
                clyde[5] = 71
            elif 0 < pacman[7] < 11:
                clyde[5] = 73
        if -4 < clyde[6] < -1:
            clyde[5] = 75
        collision()
        canevas.delete(clyde[0])
        if clyde[4] < 5:
            clyde[0] = canevas.create_image(152/32*version+clyde[1]*version,clyde[2]*version,image=allImgs[clyde[5]], anchor=NW)
        elif clyde[4] > 4:
            clyde[0] = canevas.create_image(152/32*version+clyde[1]*version,clyde[2]*version,image=allImgs[clyde[5]+1], anchor=NW)
            if clyde[4] == 9:
                clyde[4] = 0
        if clyde[7] > 0:
            clyde[7] -= 1
        if (clyde[7] == 0) & (clyde[6] != -1) & (clyde[6] > -2):
            clyde[6] = 1
        if task[0] != -1:
            if (pacman[7] > 0) & (clyde[8] == True):
                task[5] = fen.after(42, mClyde)
            elif -4 < clyde[6] < -1:
                task[5] = fen.after(15, mClyde)
            else:
                task[5] = fen.after(30, mClyde)

    def m():
        global task
        if task[0] != -1:
            task[1] = fen.after(30, mPacman)
            task[2] = fen.after(30, mBlinky)
            task[3] = fen.after(30, mPinky)
            task[4] = fen.after(30, mInky)
            task[5] = fen.after(30, mClyde)

    def haut(e):
        global pacman, task
        pacman[4] = 3
        if task == -1:
            task = 0
            m()
    def bas(e):
        global pacman, task
        pacman[4] = 1
        if task[0] == -1:
            task[0] = 0
            m()
    def gauche(e):
        global pacman, task
        pacman[4] = 2
        if task[0] == -1:
            task[0] = 0
            m()
    def droite(e):
        global pacman, task
        pacman[4] = 0
        if task[0] == -1:
            task[0] = 0
            m()

    #def debug(e):
        #global blinky, pinky, inky, clyde, debug
        #debug += 1
        #print("Debug "+str(debug))
        #print("Blinky - ",'x=',blinky[1],'y=',blinky[2],'ori=',blinky[3],"etat=",blinky[6])
        #print("Pinky - ",'x=',pinky[1],'y=',pinky[2],'ori=',pinky[3],"etat=",pinky[6])
        #print("Inky - ",'x=',inky[1],'y=',inky[2],'ori=',inky[3],"etat=",inky[6])
        #print("Clyde - ",'x=',clyde[1],'y=',clyde[2],'ori=',clyde[3],"etat=",clyde[6])
        #print("")


    #global debug
    #debug = 0
    global task
    #fen=Tk()
    fen.geometry(str(int(1200/32*version))+'x'+str(int(1000/32*version)))
    fen.title("Pacman - G-Plateforme")
    #canevas=Canvas(fen,width=1200/32*version,height=1000/32*version)
    #canevas.pack()
    nFont = font.Font(family='Arial', size=int(20/32*version), weight='bold')

    allImgs = ['']*77
    for i in range(0,77):
        allImgs[i] = PhotoImage(file = './bin/imgs/pacman/'+str(version)+'/'+str(i)+'.gif')

    fen.bind("<Up>", haut)
    fen.bind("<Down>", bas)
    fen.bind("<Left>", gauche)
    fen.bind("<Right>", droite)

    task = [0,None,None,None,None,None]

    def rejouer(e):
        global task
        task[0] = -1
        try:
            if task[1]:
                fen.after_cancel(task[1])
            if task[2]:
                fen.after_cancel(task[2])
            if task[3]:
                fen.after_cancel(task[3])
            if task[4]:
                fen.after_cancel(task[4])
            if task[5]:
                fen.after_cancel(task[5])
        except NameError:
            dump = True
        task[0] = -1
        genTab()

    genTab()

    fen.mainloop()

def stopPacman(fen, canevas):
    global task
    task[0] = -1
    try:
        if task[1]:
            fen.after_cancel(task[1])
        if task[2]:
            fen.after_cancel(task[2])
        if task[3]:
            fen.after_cancel(task[3])
        if task[4]:
            fen.after_cancel(task[4])
        if task[5]:
            fen.after_cancel(task[5])
    except NameError:
        dump = True
    task[0] = -1
    fen.unbind("<Up>")
    fen.unbind("<Down>")
    fen.unbind("<Left>")
    fen.unbind("<Right>")
    canevas.delete(ALL)
