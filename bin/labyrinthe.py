# coding: utf8
 
import os, sys

def startLabyrinthe():
    def charge_niveau(fichier):
        ligne = 0 ; case_gagnante = 0,0;
       
        labyrinthe = [[0] * 12 for _ in range(12)] # Creation liste 2 dimensions 12*12 (doit etre en accord avec le fichier lvl.txt)
       
        level = open('./bin/'+fichier, 'r')
       
        for line in level.readlines(): #  Rempli liste avec fichier texte
            for i in range(0,12):
                labyrinthe[ligne][i] = line[i]
                if line[i] == '9' :
                    case_gagnante = ligne, i
            ligne += 1
        return labyrinthe, case_gagnante
               
    def affiche_labyrinthe(labyrinthe):
        print ('\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')
        for ligne in labyrinthe :
            l = '    '
            for case in ligne :
                if case == '0' :
                    l+= '   '
                elif case == '5' :
                    l += '  X'
                elif case == '9' :
                    l += '  a' 
                else :
                    l += '  #'
            print (l)
     
    def joue_coup(labyrinthe, dir):
        x = 0; y = 0
        pos_x = 0; pos_y = 0;
        for ligne in labyrinthe :
            for case in ligne :
                if labyrinthe[x][y] == '5' :
                    pos_x = x ; pos_y = y;
                y += 1
            y = 0
            x += 1
     
        if dir == 'z' : # HAUT
            try :
                if labyrinthe[pos_x-1][pos_y] in ('0','9') :
                    labyrinthe[pos_x-1][pos_y] = '5';
                    labyrinthe[pos_x][pos_y] = '0';
            except :
                print ('Coup non légal')
       
        elif dir == 's' : # BAS
            try :
                if labyrinthe[pos_x+1][pos_y] in ('0','9') :
                    labyrinthe[pos_x+1][pos_y] = '5';
                    labyrinthe[pos_x][pos_y] = '0';
            except :
                print ('Coup non légal')
               
        elif dir == 'q' : # GAUCHE
            try :
                if labyrinthe[pos_x][pos_y-1] in ('0','9') :
                    labyrinthe[pos_x][pos_y-1] = '5';
                    labyrinthe[pos_x][pos_y] = '0';
            except :
                print ('Coup non légal')
     
        elif dir == 'd' : # DROITE
            try :
                if labyrinthe[pos_x][pos_y+1] in ('0','9') :
                    labyrinthe[pos_x][pos_y+1] = '5';
                    labyrinthe[pos_x][pos_y] = '0';
            except :
                print ('Coup non légal')
       
        return labyrinthe
               
    def main():
        labyrinthe, case_gagnante = charge_niveau('level.txt')
        while True :
            affiche_labyrinthe(labyrinthe)
            if labyrinthe[case_gagnante[0]][case_gagnante[1]] == '5' :
                print ('\n\n    Victoire ! Felicitations.')
                break
            direction = input('\n    Quelle Direction ? - Z = monter ; S = Descendre ; D = Droite ; Q = Gauche ; quitter = Quitter\n\n    >  ').lower()
     
            if direction in ('z', 's', 'd', 'q', 'quitter') :
                if direction == 'quitter' :
                    break;
                else :
                    labyrinthe = joue_coup(labyrinthe, direction)

    main()
