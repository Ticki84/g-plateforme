from tkinter import*
from random import*


def startDemineur(fen, frame, btnRetour):
    def callNouvelleGrille(e):
        nouvelleGrille()
    
    def tableauMines():
        t=[0]*12
        for i in range(0,12):
            t[i]=[0]*12
        x=randrange(1,11)
        y=randrange(1,11)
        global nbMines
        for i in range(0,nbMines):
            while t[y][x]==1:
                x=randrange(1,11)
                y=randrange(1,11)
            t[y][x]=1

        return t


    def tableauAutour(mines):
        a=[0]*12
        for i in range(0,12):
            a[i]=[0]*12
        for y in range(1,11):
            for x in range(1,11):
                a[y][x]=mines[y-1][x-1]+mines[y-1][x]+mines[y-1][x+1]+mines[y][x-1]+mines[y][x+1]+mines[y+1][x-1]+mines[y+1][x]+mines[y+1][x+1]

        return a


    def nouvelleGrille():
        global score, temps, imgsArr, perdu, mines, autour, task

        btnRecommencer.configure(state=DISABLED)
        score, temps, perdu = 0, 0, False
        lblScore.configure(text="Score : %.0f"%(score))
        lblTemps.configure(text="Temps : %.0f"%(temps))
        zone_dessin.delete(ALL)
        imgsArr = ['']*10
        for i in range(0,10):
            imgsArr[i] = ['']*10

        mines=tableauMines()
        autour=tableauAutour(mines)
            
        for i in range(1,11):
            for j in range(1,11):
                zone_dessin.create_rectangle(i*20,j*20,i*20+20,j*20+20)
                img = zone_dessin.create_image(i * 20 + 10, j * 20 + 10, image=qmark)
                imgsArr[j-1][i-1] = img

        try:
            task
        except NameError:
            task = fen.after(1000,chrono)
        else:
            fen.after_cancel(task)
            task = fen.after(1000,chrono)
        btnRecommencer.configure(state=NORMAL)


    def chrono():
        global temps, perdu, task
        if perdu == False:
            temps += 1
            lblTemps.configure(text="Temps : %.0f"%(temps))
            task = fen.after(1000,chrono)


    def decouvrir(event):
        global perdu, imgsArr, score, nbMines
        x, y = (event.x - 20) // 20 + 1, (event.y - 20) // 20 + 1
        if (perdu == False and x > 0 and x < 11 and y > 0 and y < 11):
            if mines[y][x] == 1:
                for i in range(1,11):
                    for j in range(1,11):
                        if mines[j][i] == 1:
                            zone_dessin.delete(imgsArr[j-1][i-1])
                            img = zone_dessin.create_image(i * 20 + 10, j * 20 + 10, image=bomb)
                            imgsArr[j-1][i-1] = img
                        elif autour[j][i] != -1:
                            zone_dessin.delete(imgsArr[j-1][i-1])
                            imgsArr[j-1][i-1] = ''
                            zone_dessin.create_text(i * 20 + 10, j * 20 + 10, text=autour[j][i])
                            autour[j][i] = -1
                lblScore.configure(text="Vous avez perdu! Votre score est %.0f"%(score))
                perdu = True
            else:
                if autour[y][x] != -1:
                    zone_dessin.delete(imgsArr[y-1][x-1])
                    imgsArr[y-1][x-1] = ''
                    zone_dessin.create_text(x * 20 + 10, y * 20 + 10, text=autour[y][x])
                    score += 1
                    if score < 100 - nbMines:
                        lblScore.configure(text="Score : %.0f"%(score))
                    else:
                        lblScore.configure(text="Vous avez gagné!")
                    autour[y][x] = -1


    def updateLabel(x):
        global nbMines
        nbMines = int(x)


    global score, temps, nbMines, imgsArr, qmark, bomb

    score, temps, nbMines = 0, 0, 10
    imgsArr = ['']*10
    for i in range(0,10):
        imgsArr[i] = ['']*10

    #fen=Tk()
    fen.geometry('240x410')
    fen.title("Démineur - G-Plateforme")
    zone_dessin=Canvas(frame,width=240,height=225)
    zone_dessin.pack()
    zone_dessin.bind("<Button-1>", decouvrir)
    btnRetour.pack()
    btnRecommencer=Button(frame,text="Nouvelle grille",command=nouvelleGrille)
    btnRecommencer.pack()
    lblScore=Label(frame,text  ="Score : %.0f"%(score))
    lblScore.pack()
    lblTemps=Label(frame,text ="Temps : %.0f"%(temps))
    lblTemps.pack()
    scale = Scale(frame, length=250, orient=HORIZONTAL, label ='Nombre de mines :',
            troughcolor ='dark grey', sliderlength =20,
            from_=5, to=50, tickinterval=5,
            command=updateLabel)
    scale.pack()
    scale.set(nbMines)
    scale.bind("<ButtonRelease-1>",callNouvelleGrille)

    qmark, bomb = PhotoImage(file = './bin/imgs/qmark.gif',height=20,width=20), PhotoImage(file = './bin/imgs/bomb.gif',height=20,width=20)

    nouvelleGrille()

    frame.pack()

    #fen.mainloop()

def stopDemineur(fen):
    global task
    fen.after_cancel(task)
    fen.unbind("<Button-1>")
    fen.unbind("<ButtonRelease-1>")
