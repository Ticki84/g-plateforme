from tkinter import*
from math import floor

global reste
reste=[4,3,2,1]
longueur=400
hauteur=800
longPlus=400
marg=10

def genFen():
    print('')

def motion(event):
    global ancre, reste, currOval
    x, y = event.x, event.y
    cx, cy = (x-marg)/((longueur-2*marg)/11), (y-hauteur/2-marg)/((hauteur/2-2*marg)/11)
    #print(ancre[0]+0.5-cx, ancre[1]+0.5-cy)
    dx, dy = ancre[0]+0.5-cx, ancre[1]+0.5-cy
    if dx < 0:
        dx = -dx
    if dy < 0:
        dy = -dy
    if dx > dy:
        try:
            zone_dessin.delete(currOval)
        except NameError:
            dump = True
        color = 'orange'
        if ((ancre[0]+0.5-cx < 0) | (ancre[0] == 1)) & (ancre[0] != 10):
            if (ancre[0] == 1) & (-(ancre[0]+0.5-cx) <= 1):
                dx = 1
            if round(dx) == 0:
                maxx = 10-cx
                if (maxx < 5) & (reste[0] > 0):
                    dx = 1
                elif (maxx < 5) & (reste[1] > 0):
                    dx = 2
                    color = 'purple'
                elif (maxx < 3) & (reste[2] > 0):
                    dx = 3
                    color = 'red'
                elif (maxx < 5) & (reste[3] > 0):
                    dx = 4
                    color = 'blue'
            elif round(dx) == 2:
                color = 'purple'
            elif round(dx) == 3:
                color = 'red'
            elif round(dx) > 3:
                dx = 4
                color = 'blue'
            currOval = zone_dessin.create_oval(marg+ancre[0]*((longueur-2*marg)/11),hauteur/2+marg+ancre[1]*(hauteur/2-2*marg)/11,marg+(ancre[0]+round(dx)+1)*(longueur-2*marg)/11,hauteur/2+marg+(ancre[1]+1)*(hauteur/2-2*marg)/11,outline=color,width=3,dash=(5,10))
        else:
            if (ancre[0] == 10) & (ancre[0]+0.5-cx <= 1):
                dx = 1
            if round(dx) == 0:
                if reste[0] > 0:
                    dx = 1
                elif reste[1] > 0:
                    dx = 2
                    color = 'purple'
                elif reste[2] > 0:
                    dx = 3
                    color = 'red'
                else:
                    dx = 4
                    color = 'blue'
            elif round(dx) == 2:
                color = 'purple'
            elif round(dx) == 3:
                color = 'red'
            elif round(dx) > 3:
                dx = 4
                color = 'blue'
            currOval = zone_dessin.create_oval(marg+(ancre[0]-round(dx))*((longueur-2*marg)/11),hauteur/2+marg+ancre[1]*(hauteur/2-2*marg)/11,marg+(ancre[0]+1)*(longueur-2*marg)/11,hauteur/2+marg+(ancre[1]+1)*(hauteur/2-2*marg)/11,outline=color,width=3,dash=(5,10))
        #print('x',round(dx), round(dy))
    elif dx < dy:
        if ancre[1]+0.5-cy < 0:
            dy = -dy
        #print('y',round(dx), round(dy))

def clicG(event):
    global ancre
    x, y = event.x, event.y
    if (x > marg+(longueur-2*marg)/11) & (x < marg+(longueur-2*marg)):
        if (y > marg+(hauteur/2-2*marg)/11) & (y < marg+(hauteur/2-2*marg)): #tableau haut
            print(x,y)
        elif (y > hauteur/2+marg+(hauteur/2-2*marg)/11) & (y < hauteur/2+marg+(hauteur/2-2*marg)): #tableau bas
            cx, cy = (x-marg)/((longueur-2*marg)/11), (y-hauteur/2-marg)/((hauteur/2-2*marg)/11)
            ancre = [floor(cx),floor(cy)]
            print(floor(cx),floor(cy))
            zone_dessin.bind('<Motion>', motion)


def clicD(event):
    x=event.x
    y=event.y

fen=Tk()
fen.geometry(str(longueur+longPlus) + 'x' + str(hauteur+40))
fen.title('Bataille Navale - G-Platforme')
zone_dessin=Canvas(fen,width=longueur+longPlus,height=hauteur)
zone_dessin.pack()
zone_dessin.create_line(0,hauteur/2,longueur,hauteur/2)
zone_dessin.create_line(longueur,0,longueur,hauteur)
zone_dessin.create_text(longueur+longPlus/2,hauteur/2+20,text="Navires:")
zone_dessin.create_oval(longueur+marg,hauteur/2+2*(hauteur/2-2*marg)/11,longueur+marg+5*((longueur-2*marg)/11),hauteur/2+3*(hauteur/2-2*marg)/11,outline='blue',width=3)
zone_dessin.create_text(longueur+marg+6*((longueur-2*marg)/11),hauteur/2+2.5*(hauteur/2-2*marg)/11,text='x1 - Porte-Avions (5 cases)',anchor='w',fill='red')
zone_dessin.create_oval(longueur+marg+0.5*((longueur-2*marg)/11),hauteur/2+4*(hauteur/2-2*marg)/11,longueur+marg+4.5*((longueur-2*marg)/11),hauteur/2+5*(hauteur/2-2*marg)/11,outline='red',width=3)
zone_dessin.create_text(longueur+marg+6*((longueur-2*marg)/11),hauteur/2+4.5*(hauteur/2-2*marg)/11,text='x2 - Croiseurs (4 cases)',anchor='w',fill='red')
zone_dessin.create_oval(longueur+marg+((longueur-2*marg)/11),hauteur/2+6*(hauteur/2-2*marg)/11,longueur+marg+4*((longueur-2*marg)/11),hauteur/2+7*(hauteur/2-2*marg)/11,outline='purple',width=3)
zone_dessin.create_text(longueur+marg+6*((longueur-2*marg)/11),hauteur/2+6.5*(hauteur/2-2*marg)/11,text='x3 - Sous-Marins (3 cases)',anchor='w',fill='red')
zone_dessin.create_oval(longueur+marg+1.5*((longueur-2*marg)/11),hauteur/2+8*(hauteur/2-2*marg)/11,longueur+marg+3.5*((longueur-2*marg)/11),hauteur/2+9*(hauteur/2-2*marg)/11,outline='orange',width=3)
zone_dessin.create_text(longueur+marg+6*((longueur-2*marg)/11),hauteur/2+8.5*(hauteur/2-2*marg)/11,text='x4 - Torpilleurs (2 cases)',anchor='w',fill='red')
lettres="ABCDEFGHIJ"
for i in range(1,11):
    zone_dessin.create_rectangle(marg+i*((longueur-2*marg)/11),marg,marg+(i+1)*(longueur-2*marg)/11,marg+(hauteur/2-2*marg)/11)
    zone_dessin.create_text(marg+(i+1/2)*(longueur-2*marg)/11,marg+1/2*(hauteur/2-2*marg)/11, text=i)
    zone_dessin.create_rectangle(marg+i*((longueur-2*marg)/11),hauteur/2+marg,marg+(i+1)*(longueur-2*marg)/11,hauteur/2+marg+(hauteur/2-2*marg)/11)
    zone_dessin.create_text(marg+(i+1/2)*(longueur-2*marg)/11,hauteur/2+marg+1/2*(hauteur/2-2*marg)/11, text=i)
for j in range(1,11):
    for i in range(0,11):
        zone_dessin.create_rectangle(marg+i*((longueur-2*marg)/11),marg+j*(hauteur/2-2*marg)/11,marg+(i+1)*(longueur-2*marg)/11,marg+(j+1)*(hauteur/2-2*marg)/11)
        zone_dessin.create_rectangle(marg+i*((longueur-2*marg)/11),hauteur/2+marg+j*(hauteur/2-2*marg)/11,marg+(i+1)*(longueur-2*marg)/11,hauteur/2+marg+(j+1)*(hauteur/2-2*marg)/11)
    zone_dessin.create_text(marg+1/2*(longueur-2*marg)/11,marg+(j+1/2)*(hauteur/2-2*marg)/11, text=lettres[j-1])
    zone_dessin.create_text(marg+1/2*(longueur-2*marg)/11,hauteur/2+marg+(j+1/2)*(hauteur/2-2*marg)/11, text=lettres[j-1])
btnRecommencer=Button(fen,text="Nouvelle grille")
btnRecommencer.pack()
zone_dessin.bind("<Button-1>", clicG)
zone_dessin.bind("<Button-3>", clicD)
fen.mainloop()
