from tkinter import*
from random import*


def randCouleur():
    pal=['black','dimgray','gray','darkgray','silver','lightgray','gainsboro',
         'whitesmoke','white','snow','rosybrown','lightcoral','indianred','brown',
         'firebrick','maroon','darkred','red','mistyrose','salmon','tomato',
         'darksalmon','coral','orangered','lightsalmon','sienna','seashell',
         'chocolate','saddlebrown','sandybrown','peachpuff','peru','linen',
         'bisque','darkorange','burlywood','antiquewhite','tan',
         'navajowhite','blanchedalmond','papayawhip','moccasin','orange',
         'wheat','oldlace','floralwhite','darkgoldenrod','goldenrod',
         'cornsilk','gold','lemonchiffon','khaki','palegoldenrod',
         'darkkhaki','ivory','beige','lightyellow','lightgoldenrodyellow',
         'olive','yellow','olivedrab','yellowgreen','darkolivegreen',
         'greenyellow','chartreuse','lawngreen','honeydew','darkseagreen',
         'palegreen','lightgreen','forestgreen','limegreen','darkgreen',
         'green','lime','seagreen','mediumseagreen','springgreen',
         'mintcream','mediumspringgreen','mediumaquamarine','aquamarine',
         'turquoise','lightseagreen','mediumturquoise','azure','lightcyan',
         'paleturquoise','darkslategray','teal','darkcyan','aqua','cyan',
         'darkturquoise','cadetblue','powderblue','lightblue','deepskyblue',
         'skyblue','lightskyblue','steelblue','aliceblue','dodgerblue',
         'lightslategray','slategray','lightsteelblue','cornflowerblue',
         'royalblue','ghostwhite','lavender','midnightblue','navy','darkblue',
         'mediumblue','blue','slateblue','darkslateblue','mediumslateblue',
         'mediumpurple','blueviolet','indigo','darkorchid','darkviolet',
         'mediumorchid','thistle','plum','violet','purple','darkmagenta',
         'fuchsia','purple','magenta','orchid','mediumvioletred','deeppink',
         'hotpink','lavenderblush','palevioletred','crimson','pink','lightpink']
    return pal[randrange(0,len(pal)-1)]

def startSnake(fen, frame, btnRetour):
    def genTab():
        global direction, duree, pt, corps
        direction, duree, pt, corps = -1, 600, [None, None, None, None], [[randCouleur(),8,8]]*1
        lblScore.configure(text="Score : %.0f"%(len(corps)-1))

        zone_dessin.delete(ALL)
        zone_dessin.create_rectangle(20,20,16*20,16*20)
        corps[0].append(zone_dessin.create_rectangle(8*20+2,8*20+2,8*20+18,8*20+18, fill=corps[0][0]))
        genPt()

    def genPt():
        global corps, pt
        zone_dessin.delete(pt[3])
        pt, x, y, occ = [None, None, None, None], randrange(1,16), randrange(1,16), False
        while True:
            for i in range(0,len(corps)):
                if (corps[i][1] == x) & (corps[i][2] == y):
                    occ = True
            if occ == True:
                x, y = randrange(1,16), randrange(1,16)
                occ = False
            else:
                break
        couleur = randCouleur()
        pt = [couleur, x, y, zone_dessin.create_oval(x*20+5,y*20+5,x*20+15,y*20+15, fill=couleur)]

    def seDeplace():
        global corps, pt, direction, task, duree
        if direction != -1:
            x, y, mange, occ = corps[0][1], corps[0][2], False, False
            if direction == 0:
                if (x == pt[1]) & ((y-1 == pt[2]) | ((y-1 < 1) & (pt[2] == 15))):
                    mange = True
                    y -= 1
                    if y < 1:
                        y = 15
                    corps.insert(0,[pt[0],x,y,zone_dessin.create_rectangle(x*20+2,y*20+2,x*20+18,y*20+18,fill=pt[0])])
                    duree = int(duree * 4/5)
                    genPt()
                for i in range(0,len(corps)):
                    if (x == corps[i][1]) & ((y-1 == corps[i][2]) | ((y-1 < 1) & (corps[i][2] == 15))):
                        occ = True
                        break
            elif direction == 1:
                if (x == pt[1]) & ((y+1 == pt[2]) | ((y+1 > 15) & (pt[2] == 1))):
                    mange = True
                    y += 1
                    if y > 15:
                        y = 1
                    corps.insert(0,[pt[0],x,y,zone_dessin.create_rectangle(x*20+2,y*20+2,x*20+18,y*20+18,fill=pt[0])])
                    duree = int(duree * 4/5)
                    genPt()
                for i in range(0,len(corps)):
                    if (x == corps[i][1]) & ((y+1 == corps[i][2]) | ((y+1 > 15) & (corps[i][2] == 1))):
                        occ = True
                        break
            elif direction == 2:
                if (y == pt[2]) & ((x-1 == pt[1]) | ((x-1 < 1) & (pt[1] == 15))):
                    mange = True
                    x -= 1
                    if x < 1:
                        x = 15
                    corps.insert(0,[pt[0],x,y,zone_dessin.create_rectangle(x*20+2,y*20+2,x*20+18,y*20+18,fill=pt[0])])
                    duree = int(duree * 4/5)
                    genPt()
                for i in range(0,len(corps)):
                    if (y == corps[i][2]) & ((x-1 == corps[i][1]) | ((x-1 < 1) & (corps[i][1] == 15))):
                        occ = True
                        break
            elif direction == 3:
                if (y == pt[2]) & ((x+1 == pt[1]) | ((x+1 > 15) & (pt[1] == 1))):
                    mange = True
                    x += 1
                    if x > 15:
                        x = 1
                    corps.insert(0,[pt[0],x,y,zone_dessin.create_rectangle(x*20+2,y*20+2,x*20+18,y*20+18,fill=pt[0])])
                    duree = int(duree * 4/5)
                    genPt()
                for i in range(0,len(corps)):
                    if (y == corps[i][2]) & ((x+1 == corps[i][1]) | ((x+1 > 15) & (corps[i][1] == 1))):
                        occ = True
                        break
            lblScore.configure(text="Score : %.0f"%(len(corps)-1))
            if duree < 51:
                duree = 50
            if (mange == False) & (occ == False):
                length = len(corps)-1
                for i in range(0,length+1):
                    if i == length:
                        if direction == 0:
                            corps[0][2] = corps[0][2]-1
                            if corps[0][2] < 1:
                                corps[0][2] = 15
                        elif direction == 1:
                            corps[0][2] = corps[0][2]+1
                            if corps[0][2] > 15:
                                corps[0][2] = 1
                        elif direction == 2:
                            corps[0][1] = corps[0][1]-1
                            if corps[0][1] < 1:
                                corps[0][1] = 15
                        elif direction == 3:
                            corps[0][1] = corps[0][1]+1
                            if corps[0][1] > 15:
                                corps[0][1] = 1
                    else:
                        corps[length-i][1] = corps[length-i-1][1]
                        corps[length-i][2] = corps[length-i-1][2]
                    zone_dessin.delete(corps[length-i][3])
                    corps[length-i][3] = zone_dessin.create_rectangle(corps[length-i][1]*20+2,corps[length-i][2]*20+2,corps[length-i][1]*20+18,corps[length-i][2]*20+18, fill=corps[length-i][0])
            elif occ == True:
                genTab()
            if occ == False:
                task = fen.after(duree,seDeplace)

    def haut(ev):
        global direction, task, duree, corps
        if direction == -1:
            task = fen.after(duree,seDeplace)
        if len(corps) > 1:
            if (corps[0][2]-corps[1][2] != 1) & (corps[0][2]-corps[1][2] != -14):
                direction = 0
        else:
            direction = 0
    def bas(ev):
        global direction, task, duree, corps
        if direction == -1:
            task = fen.after(duree,seDeplace)
        if len(corps) > 1:
            if (corps[0][2]-corps[1][2] != -1) & (corps[0][2]-corps[1][2] != 14):
                direction = 1
        else:
            direction = 1
    def gauche(ev):
        global direction, task, duree, corps
        if direction == -1:
            task = fen.after(duree,seDeplace)
        if len(corps) > 1:
            if (corps[0][1]-corps[1][1] != 1) & (corps[0][1]-corps[1][1] != -14):
                direction = 2
        else:
            direction = 2
    def droite(ev):
        global direction, task, duree, corps
        if direction == -1:
            task = fen.after(duree,seDeplace)
        if len(corps) > 1:
            if (corps[0][1]-corps[1][1] != -1) & (corps[0][1]-corps[1][1] != 14):
                direction = 3
        else:
            direction = 3
    

    #fen=Tk()
    fen.geometry('400x420')
    fen.title("Snake - G-Plateforme")
    zone_dessin=Canvas(frame,width=340,height=340)
    zone_dessin.pack()
    btnRetour.pack()
    btnRecommencer=Button(frame,text="Rejouer",command=genTab)
    btnRecommencer.pack()
    lblScore=Label(frame,text  ="Score : %.0f"%(0))
    lblScore.pack()

    fen.bind("<Up>", haut)
    fen.bind("<Down>", bas)
    fen.bind("<Left>", gauche)
    fen.bind("<Right>", droite)

    genTab()

    frame.pack()

    #fen.mainloop()

def stopSnake(fen):
    global task
    try:
        fen.after_cancel(task)
    except NameError:
        dump = True
    fen.unbind("<Up>")
    fen.unbind("<Down>")
    fen.unbind("<Left>")
    fen.unbind("<Right>")
