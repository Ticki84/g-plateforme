from tkinter import*
from tkinter import font

import os
#os.chdir("D:\\Programmation\\ISN\\Projet\Programme")
from bin.snake import startSnake, stopSnake, randCouleur
from bin.demineur import startDemineur, stopDemineur
from bin.pacman import startPacman, stopPacman
from bin.horreur import startHorreur
from bin.labyrinthe import startLabyrinthe


def snake():
    global fen, task, frame
    def retour():
        stopSnake(fen)
        nFrame.destroy()
        menu()
    fen.after_cancel(task)
    frame.destroy()
    nFrame = Frame(fen)
    btnRetour = Button(nFrame,text="Retour",command=retour)
    startSnake(fen, nFrame, btnRetour)

def demineur():
    global fen, task, frame
    def retour():
        stopDemineur(fen)
        nFrame.destroy()
        menu()
    fen.after_cancel(task)
    frame.destroy()
    nFrame = Frame(fen)
    btnRetour = Button(nFrame,text="Retour",command=retour)
    startDemineur(fen, nFrame, btnRetour)

def pacman():
    global fen, task, frame
    def retour(e):
        global fen
        stopPacman(nFenetre,canevas)
        nFenetre.destroy()
        fen = Tk()
        fen.focus_force()
        menu()
    fen.after_cancel(task)
    frame.destroy()
    fen.destroy()
    nFenetre=Tk()
    nFenetre.focus_force()
    canevas = Canvas(nFenetre,width=1200/32*24,height=1000/32*24)
    canevas.pack()
    fond = canevas.create_rectangle(0,0,1200/32*24,24*31, fill="black")
    txtRetour = canevas.create_text(1124/32*24,560/32*24, text="RETOUR", fill="pink", activefill="white", font=font.Font(family='Arial', size=int(20/32*24), weight='bold'))
    canevas.tag_bind(txtRetour, '<Button-1>', retour)
    startPacman(nFenetre, canevas, fond, txtRetour, 24)

def pacmanMaxi():
    global fen, task, frame
    def retour(e):
        global fen
        stopPacman(nFenetre,canevas)
        nFenetre.destroy()
        fen = Tk()
        fen.focus_force()
        menu()
    fen.after_cancel(task)
    frame.destroy()
    fen.destroy()
    nFenetre=Tk()
    nFenetre.focus_force()
    canevas = Canvas(nFenetre,width=1200,height=1000)
    canevas.pack()
    fond = canevas.create_rectangle(0,0,1200,32*31, fill="black")
    txtRetour = canevas.create_text(1124,560, text="RETOUR", fill="pink", activefill="white", font=font.Font(family='Arial', size=int(20/32*24), weight='bold'))
    canevas.tag_bind(txtRetour, '<Button-1>', retour)
    startPacman(nFenetre, canevas, fond, txtRetour, 32)

def horreur():
    global fen, task, frame
    fen.after_cancel(task)
    frame.destroy()
    nFrame = Frame(fen)
    canevasLogo = Canvas(nFrame, height=150, width=400)
    canevasLogo.background = PhotoImage(file = './bin/imgs/logo.gif',height=50,width=300)
    canevasLogo.image = canevasLogo.create_image(150, 25, image=canevasLogo.background)
    canevasLogo.grid(row=0, columnspan=2, padx=0, pady=5)
    canevasLogo.create_text(150, 100, text="Regardez la console")
    nFrame.pack()
    fen.after(100, startHorreur)
    fen.after(150, nFrame.destroy)
    fen.after(200, menu)

def labyrinthe():
    global fen, task, frame
    fen.after_cancel(task)
    frame.destroy()
    nFrame = Frame(fen)
    canevasLogo = Canvas(nFrame, height=150, width=400)
    canevasLogo.background = PhotoImage(file = './bin/imgs/logo.gif',height=50,width=300)
    canevasLogo.image = canevasLogo.create_image(150, 25, image=canevasLogo.background)
    canevasLogo.grid(row=0, columnspan=2, padx=0, pady=5)
    canevasLogo.create_text(150, 100, text="Regardez la console")
    nFrame.pack()
    fen.after(100, startLabyrinthe)
    fen.after(150, nFrame.destroy)
    fen.after(200, menu)

def changerCouleur():
    global fen, btnSnake, task
    btnSnake.configure(fg=randCouleur())
    task = fen.after(100, changerCouleur)

def menu():
    global fen, frame, btnSnake
    fen.geometry('300x180')
    fen.title("Menu Principal - G-Plateforme")
    frame = Frame(fen)
    canevasLogo = Canvas(frame, height=50, width=300)
    canevasLogo.background = PhotoImage(file = './bin/imgs/logo.gif',height=50,width=300)
    canevasLogo.image = canevasLogo.create_image(150, 25, image=canevasLogo.background)
    canevasLogo.grid(row=0, columnspan=2, padx=0, pady=5)
    btnSnake = Button(frame,text='Snake',command=snake,width=15,fg=randCouleur())
    btnSnake.grid(row=1, column=0, padx=10, pady=5)
    btnDemineur = Button(frame,text='Démineur',command=demineur,width=15,fg='red')
    btnDemineur.grid(row=1, column=1, padx=10, pady=5)
    btnPacman = Button(frame,text='Pacman',command=pacman,width=15,fg='orange')
    btnPacman.grid(row=2, column=0, padx=10, pady=5)
    btnPacmanMaxi = Button(frame,text='Maxi Pacman',command=pacmanMaxi,width=15,fg='darkorange')
    btnPacmanMaxi.grid(row=2, column=1, padx=10, pady=5)
    btnHorreur = Button(frame,text='Horreur',command=horreur,width=15,fg='red')
    btnHorreur.grid(row=3, column=0, padx=10, pady=5)
    btnLabyrinthe = Button(frame,text='Labyrinthe',command=labyrinthe,width=15,fg='green')
    btnLabyrinthe.grid(row=3, column=1, padx=10, pady=5)
    frame.pack()
    changerCouleur()

global fen
fen = Tk()
menu()
mainloop()